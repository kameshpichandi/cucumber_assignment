package com.testngcucumber;

import java.time.Duration;

import org.testng.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class LoginSteps {
	static WebDriver driver = null;

	@Given("User has launch the demowebshop Url")
	public void user_has_launch_the_demowebshop_url() {
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
		driver.get("https://demowebshop.tricentis.com/login");

	}

	@When("user has enter the valid username to the email text field")
	public void user_has_enter_the_valid_username_to_the_email_text_field() {

		driver.findElement(By.id("Email")).sendKeys("kameshkam3094@gmail.com");

	}

	@When("user has enter the valid password to the password text field")
	public void user_has_enter_the_valid_password_to_the_password_text_field() {

		driver.findElement(By.id("Password")).sendKeys("Kamesh@30");
	}

	@When("click the login button")
	public void click_the_login_button() {

		driver.findElement(By.xpath("//input[@value='Log in']")).click();
	}

	@Then("user is landing on Home page")
	public void user_is_landing_on_home_page() {

		String title = "Demo Web Shop";
		Assert.assertEquals(title, driver.getTitle());
		System.out.println("Home Page Title :" + driver.getTitle());
	}

	@Then("click on the logout button")
	public void click_on_the_logout_button() {
		driver.findElement(By.xpath("//a[normalize-space()='Log out']")).click();

	}

	@Then("user should taken to the login page of demowebshop application")
	public void user_should_taken_to_the_login_page_of_demowebshop_application() {

		boolean loginPageLog = driver.findElement(By.xpath("//img[@alt='Tricentis Demo Web Shop']")).isDisplayed();
		Assert.assertEquals(true, loginPageLog);

	}

	@Then("close the browser")
	public void close_the_browser() {
		driver.quit();
	}
	@Then("click on computer page and navigate to destop page")
	public void click_on_computer_page_and_navigate_to_destop_page() {
		WebElement computer = driver.findElement(By.xpath("//ul[@class='top-menu']//a[contains(text(),'Computers')]"));
		Actions ac = new Actions(driver);
		ac.moveToElement(computer).perform();
		driver.findElement(By.xpath("//ul[@class='top-menu']//a[normalize-space()='Desktops']")).click();

	}

	@Then("sort by price low to high")
	public void sort_by_price_low_to_high() {
		WebElement sortBy = driver.findElement(By.id("products-orderby"));
		Select sc = new Select(sortBy);
		sc.selectByVisibleText("Price: Low to High");

	}

	@Then("click on add to cart and go to shopping cart")
	public void click_on_add_to_cart_and_go_to_shopping_cart() {
		driver.findElement(By.xpath("(//div[@class='buttons']//input[@value='Add to cart'])[1]")).click();
		driver.findElement(By.id("add-to-cart-button-72")).click();
		driver.findElement(By.xpath("//span[contains(text(),'Shopping cart')]")).click();

	}

	@Then("click on chekout")
	public void click_on_chekout() {

		driver.findElement(By.id("termsofservice")).click();
		driver.findElement(By.id("checkout")).click();

	}


}
