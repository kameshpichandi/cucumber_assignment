package com.testngcucumber;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
				features = "src/test/resources/features",
				glue = "com.testngcucumber",
				plugin = {"pretty","html:target/cucumber-reports"},
				monochrome = true,
				dryRun = false,
				tags = "@smoktest"
				
				
				
		
		
		
		)

public class TestRunnerClass extends AbstractTestNGCucumberTests {

}
